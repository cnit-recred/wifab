#!/bin/bash

if [ "$WIFABLE_DEBUG" == "1" ]; then
	set -x
fi

PSK_CHANGE_TIME=20 # seconds
PSK_INDEX_MODULE=256 # maximum key index + 1
NSTADISPLAYINTERVAL=4
HOSTAPD=../hostap/hostapd/hostapd
HOSTAPD_CONF=hostapd.conf
HOSTAPD_PSK_FILE=/tmp/hostapd.wpa_psk
FIFO=/tmp/ssidpipe
POURINTHEPIPE=../fountaincode/pourinthepipe_nofc.py
POURINTHEPIPEPIDFILE=/tmp/pourinthepipe.pid
POURINTHEPIPECHANGEFILE=/tmp/pourinthepipe.change
CPABE_ENC=../cpabe/cpabe-enc
CPABE_PUB_KEY=./pub_key
# the destination file should be the one used by pourinthepipe.py
ENCRYPTED_PSK_FILE=./psk.cpabe
UNENCRYPTED_PSK_FILE=./psk
CPABE_POLICY="student or professor"
DEV=myap

generate_psk() {
    dd if=/dev/urandom bs=256 count=1 status=none | sha256sum | awk '{print $1}'
}

if [ ! -e $FIFO ]; then
	mkfifo $FIFO 2>&1 >/dev/null
fi

echo 4096 > /proc/sys/fs/pipe-max-size 2>/dev/null

NEWPSK=$(generate_psk)
echo $NEWPSK > $UNENCRYPTED_PSK_FILE
$CPABE_ENC "$CPABE_PUB_KEY" "$UNENCRYPTED_PSK_FILE" "$CPABE_POLICY" -o $ENCRYPTED_PSK_FILE 2>&1 >/dev/null
OLDPSK="$NEWPSK"

# kill all the old processes that are still there
pgrep -a python2 | grep pourinthepipe | awk '{print $1}' | while read p; do kill $p; done 2>&1 >/dev/null
touch $POURINTHEPIPECHANGEFILE
$POURINTHEPIPE > /tmp/pour.log 2>&1 &
$HOSTAPD $HOSTAPD_CONF > /tmp/hostapd.log 2>&1 &

while true; do
	sleep $NSTADISPLAYINTERVAL
	NSTA=$( iw dev $DEV station dump | grep Station | wc -l )
    echo "----> Number of connected clients: $NSTA"
done &

while true; do
    STARTTIME=$(date '+%s')

    # generate a new password
    NEWPSK=$(generate_psk)
    echo "----> Generate a new WPA password: " $NEWPSK

    # put it in a file and encrypt it
    echo $NEWPSK > $UNENCRYPTED_PSK_FILE
    echo "----> Encrypt the WPA password using CP-ABE"
    $CPABE_ENC "$CPABE_PUB_KEY" "$UNENCRYPTED_PSK_FILE" "$CPABE_POLICY" -o $ENCRYPTED_PSK_FILE 2>&1 >/dev/null

    # update the hostapd.wpa_psk file
    echo "----> Change the WPA password on the Access Point"
    echo "00:00:00:00:00:00 ${OLDPSK}"  > $HOSTAPD_PSK_FILE
    echo "00:00:00:00:00:00 ${NEWPSK}" >> $HOSTAPD_PSK_FILE
	#cat $HOSTAPD_PSK_FILE

    # send a SIGHUP to hostapd to make it reload its config
    # TODO: check if we can use a pidfile
    pkill -HUP hostapd 2>&1 >/dev/null

    echo "----> Use fountain coding to split the encrypted WPA password in chunks, and transmit them in beacons"
    # make pourinthepipe.py reload its data source file
	touch $POURINTHEPIPECHANGEFILE

	# try to drain the pipe
	dd if=${FIFO} of=/dev/null bs=514 count=$(( 100 * $(cat /proc/sys/fs/pipe-max-size) / 514 )) iflag=fullblock status=none

    OLDPSK=${NEWPSK}

    ENDTIME=$(date '+%s')
    # sleep for the remaining time
    sleep $(( $PSK_CHANGE_TIME - ( $ENDTIME - $STARTTIME ) ))
done

