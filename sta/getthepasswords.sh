#!/bin/bash

if [ "$WIFABLE_DEBUG" == "1" ]; then
	set -x
fi

IW=../iw/iw
DEV=wlan2
FIFO=/tmp/ssidpipe2
CPABE_DEC=../cpabe/cpabe-dec
CPABE_PUB_KEY=./pub_key
CPABE_PRIV_KEY=./stud1_key
ENCRYPTED_PSK_FILE=/tmp/psk.cpabe
UNENCRYPTED_PSK_FILE=/tmp/psk
TARGET_SSID="ctest"
TARGET_FREQ=2412

generate_wpa_supplicant_conf () {
	PSK="$1"
	TARGET_FILE="$2"
	echo "network={                       "  > ${TARGET_FILE}
	echo "        ssid=\"${TARGET_SSID}\" " >> ${TARGET_FILE} 
	echo "        psk=${PSK}              " >> ${TARGET_FILE}
	echo "}                               " >> ${TARGET_FILE} 
}

if [ ! -e $FIFO ]; then
	mkfifo $FIFO 2>&1 >/dev/null
fi

rm -f $ENCRYPTED_PSK_FILE
rm -f $UNENCRYPTED_PSK_FILE

pgrep -a python2 | grep filltheglassfromthepipe | awk '{print $1}' | while read p; do kill $p; done 2>&1 >/dev/null

ip link set $DEV up 2>&1 >/dev/null

t0=$(date +%s)

# TODO: change the final condition
while [ ! -e "$UNENCRYPTED_PSK_FILE" ]; do
    echo "----> Capture fountain coded chunk in beacon"
	echo "" > $FIFO
    $IW dev $DEV scan freq $TARGET_FREQ ssid "$TARGET_SSID" | grep endor | grep '00:0c:42' | grep -o "data:.*$" | sed 's/data: //' > $FIFO
    sleep 0.1
done &


#touch $BASE64_PSK_FILE
#
#inotifywait -e modify $BASE64_PSK_FILE

while [ ! -e $UNENCRYPTED_PSK_FILE ]; do
    ../fountaincode/filltheglassfromthepipe.py 2>&1 >/tmp/fill.log
    echo "----> Encrypted WPA password received"
    echo "----> Attempt to decrypt the encrypted WPA password"
    $CPABE_DEC $CPABE_PUB_KEY $CPABE_PRIV_KEY $ENCRYPTED_PSK_FILE -o $UNENCRYPTED_PSK_FILE 2>&1 >/dev/null
	sleep 0.1
done

t1=$(date +%s)
echo "----> WPA password caught and decoded in $(( $t1 - $t0 )) seconds"
echo "----> The WPA password is: $(cat $UNENCRYPTED_PSK_FILE)"

echo "----> Generate WPA supplicant configuration"
generate_wpa_supplicant_conf "$(cat $UNENCRYPTED_PSK_FILE)" "/tmp/wpa_supplicant.conf" 2>&1 >/dev/null

echo "----> Connect to the Access Point"
wpa_supplicant -i${DEV} -c /tmp/wpa_supplicant.conf -q

