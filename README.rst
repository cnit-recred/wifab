WI-FAB
------

This is the WI-FAB repository.

For more information please refer to:

- Claudio Pisa, Alberto Caponi, Tooska Dargahi, Giuseppe Bianchi, Nicola Blefari-Melazzi. “WI-FAB: attribute-based WLAN access control, without pre-shared keys and backend infrastructures.” HotPOST 2016
- Claudio Pisa, Tooska Dargahi, Alberto Caponi, Giuseppe Bianchi, Nicola Blefari-Melazzi “On the feasibility of attribute-based encryption for WLAN access control.” WiMob 2017

For OpenWRT/LEDE packages check also: https://bitbucket.org/cnit-recred/openwrt-lede-wifab/

